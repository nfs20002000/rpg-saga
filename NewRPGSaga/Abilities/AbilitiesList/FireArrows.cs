﻿namespace NewRPGSaga.Abilities.AbilitiesList
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Effects.EffectsList;
    using NewRPGSaga.Interfaces;

    public class FireArrows : IAbility
    {
        private const int FireArrowsDuration = 3;

        public void DoSpecialAction(Character attacker, Character defender)
        {
            Logger.Log($"{attacker.GetType().Name} {attacker.Name} uses the Fire Arrows!", ConsoleColor.Green);
            defender.EffectsPool.Add(new FireArrowsEffect(FireArrowsDuration));
        }
    }
}
