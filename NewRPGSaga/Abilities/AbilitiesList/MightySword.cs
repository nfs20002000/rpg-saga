﻿namespace NewRPGSaga.Abilities.AbilitiesList
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Interfaces;

    public class MightySword : IAbility
    {
        private const double MightySwordDamage = 1.3;

        public void DoSpecialAction(Character attacker, Character defender)
        {
            Logger.Log(
                $"{attacker.GetType().Name} {attacker.Name} uses his mighty sword! \n" +
                $" He hits {defender.Name} for {attacker.Power * MightySwordDamage} health points!", ConsoleColor.Green);
            defender.Health -= attacker.Power * MightySwordDamage;
        }
    }
}
