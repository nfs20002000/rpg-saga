﻿namespace NewRPGSaga.Abilities.AbilitiesList
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Effects.EffectsList;
    using NewRPGSaga.Interfaces;

    public class MistyFog : IAbility
    {
        private const int MistyFogDuration = 2;

        public void DoSpecialAction(Character attacker, Character defender)
        {
            Logger.Log($"{attacker.GetType().Name} {attacker.Name} uses the Misty Fog!", ConsoleColor.Green);
            defender.EffectsPool.Add(new MistyFogEffect(MistyFogDuration));
        }
    }
}
