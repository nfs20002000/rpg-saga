﻿namespace NewRPGSaga.Abilities
{
    using System;
    using NewRPGSaga.Characters;

    public static class AbilitiesLogic
    {
        public static bool PerformAbility(Character attacker, Character defender)
        {
            if (attacker.AbilitiesPool != null && attacker.AbilitiesPool.Count > 0)
            {
                Random rand = new Random();
                int abilityNumber = rand.Next(0, attacker.AbilitiesPool.Count);
                attacker.AbilitiesPool[abilityNumber].DoSpecialAction(attacker, defender);
                return defender.GetStatusAfterAction();
            }

            return false;
        }
    }
}
