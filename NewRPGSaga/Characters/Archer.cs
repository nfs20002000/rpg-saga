﻿namespace NewRPGSaga.Characters
{
    using System.Collections.Generic;
    using NewRPGSaga.Interfaces;

    public class Archer : Character
    {
        public Archer(string name, double health, int power, List<IAbility> abilities = null)
            : base(name, health, power, abilities)
        {
        }
    }
}
