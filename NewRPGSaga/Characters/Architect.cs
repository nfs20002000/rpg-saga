﻿namespace NewRPGSaga.Characters
{
    using NewRPGSaga.Interfaces;
    using System.Collections.Generic;

    public class Architect : Character
    {
        public Architect(string name, double health, int power, List<IAbility> abilities = null)
            : base(name, health, power, abilities)
        {
        }
    }
}
