﻿namespace NewRPGSaga.Characters
{
    using System.Collections.Generic;
    using NewRPGSaga;
    using NewRPGSaga.Interfaces;

    public abstract class Character
    {
        public Character(string name, double health, int power, List<IAbility> abilities = null)
        {
            this.Name = name;
            this.Health = health;
            this.Power = power;
            this.AbilitiesPool = abilities;
            this.EffectsPool = new List<IEffect>();
            this.IsAlive = true;
            this.IsAvailiable = true;
        }

        public string Name { get; set; }

        public double Health { get; set; }

        public int Power { get; set; }

        public bool IsAlive { get; set; }

        public bool IsAvailiable { get; set; }

        public List<IAbility> AbilitiesPool { get; set; }

        public List<IEffect> EffectsPool { get; set; }

        public bool CheckHealth()
        {
            return this.Health > 0;
        }

        public bool GetStatusAfterAction()
        {
            if (!CheckHealth())
            {
                this.IsAlive = false;
                Logger.Log($"{this.Name} is finished =(\n");
                return true;
            }

            return false;
        }

        public override string ToString()
        {
            return $"[Type: {this.GetType().Name}, Name: {this.Name}, Health: {this.Health}, Power: {this.Power}]";
        }
    }
}
