﻿namespace NewRPGSaga.Characters
{
    using NewRPGSaga.Interfaces;
    using System.Collections.Generic;

    public class Knight : Character
    {
        public Knight(string name, double health, int power, List<IAbility> abilities = null)
            : base(name, health, power, abilities)
        {
        }
    }
}
