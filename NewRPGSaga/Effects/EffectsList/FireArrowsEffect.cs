﻿namespace NewRPGSaga.Effects.EffectsList
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Interfaces;

    public class FireArrowsEffect : IEffect
    {
        private const int FireDamage = 5;

        private int movesRemain;

        public FireArrowsEffect(int movesRemain)
        {
            this.MovesRemain = movesRemain;
        }

        public int MovesRemain { get => movesRemain; set => movesRemain = value >= 0 ? value : 1; }

        public void PerformEffect(Character character)
        {
            Logger.Log($"{character.GetType().Name} {character.Name} is burning! It's getting -{FireDamage} HP!", ConsoleColor.Yellow);
            character.Health -= FireDamage;
            MovesRemain--;
        }
    }
}
