﻿namespace NewRPGSaga.Effects.EffectsList
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Interfaces;

    public class MistyFogEffect : IEffect, IMoveSkipable
    {
        private int movesRemain;

        public MistyFogEffect(int movesRemain)
        {
            this.MovesRemain = movesRemain;
        }

        public int MovesRemain { get => movesRemain; set => movesRemain = value >= 0 ? value : 1; }

        public void PerformEffect(Character character)
        {
            Logger.Log($"{character.GetType().Name} {character.Name} is under the Misty Fog! (skip move)", ConsoleColor.Yellow);
            MovesRemain--;
        }
    }
}
