﻿namespace NewRPGSaga.Effects
{
    using System.Linq;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Interfaces;

    public static class EffectsLogic
    {
        public static bool PerformingEffects(Character character)
        {
            foreach (IEffect effect in character.EffectsPool)
            {
                effect.PerformEffect(character);
                if (character.GetStatusAfterAction())
                {
                    return true;
                }
            }

            CleanUpEffects(character);
            return false;
        }

        public static void CleanUpEffects(Character character)
        {
            character.EffectsPool.RemoveAll(effect => effect.MovesRemain == 0);
        }

        public static bool MoveSkipDetection(Character character)
        {
            return character.EffectsPool.OfType<IMoveSkipable>().Any();
        }
    }
}
