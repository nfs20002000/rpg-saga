﻿namespace NewRPGSaga.Exceptions
{
    using System;

    public class NonValidNumberException : Exception
    {
        public NonValidNumberException()
        {
            Console.WriteLine("Even number expected!");
        }
    }
}
