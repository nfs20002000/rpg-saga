﻿namespace NewRPGSaga
{
    using System;
    using System.Collections.Generic;
    using NewRPGSaga.Abilities.AbilitiesList;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Initializers;
    using NewRPGSaga.Interfaces;
    using NewRPGSaga.MainProcesses;
    using NewRPGSaga.UserActions;

    public class Game
    {
        public const int CharacterTypesNumber = 4;

        public Game()
        {
            Characters = new List<Character>();
        }

        public int NumberOfCharacters { get; set; }

        public List<Character> Characters { get; set; }

        public Round CurrentRound { get; set; }

        public void Initialize()
        {
            Random rand = new Random();
            Characters = new List<Character>(NumberOfCharacters);

            for (int i = 0; i < NumberOfCharacters; i++)
            {
                switch (rand.Next(0, CharacterTypesNumber))
                {
                    case 0:
                        {
                            Characters.Add(new Wizard(
                                CharacterNames.Wizards[rand.Next(0, CharacterNames.Wizards.Length)],
                                rand.Next(50, 100),
                                rand.Next(5, 11),
                                new List<IAbility> { new MistyFog() }));
                            break;
                        }

                    case 1:
                        {
                            Characters.Add(new Archer(
                                CharacterNames.Archers[rand.Next(0, CharacterNames.Archers.Length)],
                                rand.Next(60, 90),
                                rand.Next(7, 16),
                                new List<IAbility> { new FireArrows() }));
                            break;
                        }

                    case 2:
                        {
                            Characters.Add(new Knight(
                                CharacterNames.Knights[rand.Next(0, CharacterNames.Knights.Length)],
                                rand.Next(70, 150),
                                rand.Next(10, 21),
                                new List<IAbility> { new MightySword() }));
                            break;
                        }

                    case 3:
                        {
                            Characters.Add(new Architect(
                                CharacterNames.Architects[rand.Next(0, CharacterNames.Architects.Length)],
                                rand.Next(80, 160),
                                rand.Next(15, 31),
                                new List<IAbility> { new MightySword(), new FireArrows(), new MistyFog() }));
                            break;
                        }
                }

                Characters[i].IsAlive = true;
                Characters[i].IsAvailiable = true;

                Console.WriteLine(Characters[i].ToString());
            }
        }

        public void MainCycle()
        {
            while (Characters.Count > 1)
            {
                CurrentRound = new Round(Characters);
                CurrentRound.Start();
            }
        }

        public void WinnerCongratulation()
        {
            Logger.Log($"{Characters[0].Name} has won the competition! Congratulations!!!", ConsoleColor.Red);
        }

        public void Start()
        {
            NumberOfCharacters = UserInput.HandleCharactersNumberInput(-1);
            Initialize();
            MainCycle();
            WinnerCongratulation();
        }
    }
}
