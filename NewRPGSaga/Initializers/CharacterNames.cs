﻿namespace NewRPGSaga.Initializers
{
    public static class CharacterNames
    {
        public static readonly string[] Wizards = { "Gandalf", "Dumbledor", "Saruman", "Voldemort", "Grindelwald", "Antonidas", "Gul'Dan" };

        public static readonly string[] Archers = { "Legolas", "Ossir", "Hawkeye", "Stormtrooper" };

        public static readonly string[] Knights = { "Tirion Fordring", "Aragorn", "Gimli", "Uther", "Boromir", "Faramir", "Eomer" };

        public static readonly string[] Architects = { "Chuck Norris", "Batman", "Superman", "Test Testovich", "Test Hyperstovich" };
    }
}
