﻿namespace NewRPGSaga.Interfaces
{
    using NewRPGSaga.Characters;

    public interface IAbility
    {
        public void DoSpecialAction(Character attacker, Character defender);
    }
}
