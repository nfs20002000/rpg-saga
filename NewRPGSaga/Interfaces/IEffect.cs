﻿namespace NewRPGSaga.Interfaces
{
    using NewRPGSaga.Characters;

    public interface IEffect
    {
        public int MovesRemain { get; set; }

        public void PerformEffect(Character character);
    }
}
