﻿namespace NewRPGSaga
{
    using System;

    public static class Logger
    {
        public const ConsoleColor MainColor = ConsoleColor.White;

        public static void Log(string message, ConsoleColor messageColor = ConsoleColor.White)
        {
            Console.ForegroundColor = messageColor;
            Console.WriteLine(message);
            Console.ForegroundColor = MainColor;
        }
    }
}
