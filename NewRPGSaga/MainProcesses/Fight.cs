﻿namespace NewRPGSaga.MainProcesses
{
    using System;
    using NewRPGSaga.Abilities;
    using NewRPGSaga.Characters;
    using NewRPGSaga.Effects;

    public class Fight
    {
        public Fight(Character firstFighter, Character secondFighter)
        {
            FirstFighter = firstFighter;
            SecondFighter = secondFighter;
        }

        public Character FirstFighter { get; }

        public Character SecondFighter { get; }

        public void CleanUpAfterFight()
        {
            if (FirstFighter.IsAlive == true)
            {
                FirstFighter.EffectsPool.Clear();
            }
            else
            {
                SecondFighter.EffectsPool.Clear();
            }
        }

        public bool CommonHit(Character defender, Character attacker)
        {
            defender.Health -= attacker.Power;

            Logger.Log($"{attacker.Name} " + $"hitted {defender.Name} for {attacker.Power} HP!\n");

            return defender.GetStatusAfterAction();
        }

        public bool TryAttack(Character defender, Character attacker)
        {
            if (attacker.IsAlive == false)
            {
                return false;
            }

            Random rand = new Random();

            if (EffectsLogic.PerformingEffects(attacker))
            {
                return true;
            }

            if (EffectsLogic.MoveSkipDetection(attacker))
            {
                return false;
            }

            if (rand.Next(1, 6) < 4)
            {
                return CommonHit(defender, attacker);
            }
            else
            {
                return AbilitiesLogic.PerformAbility(attacker, defender);
            }
        }

        public void FightIteration()
        {
            while (FirstFighter.IsAlive && SecondFighter.IsAlive)
            {
                TryAttack(SecondFighter, FirstFighter);
                TryAttack(FirstFighter, SecondFighter);
            }
        }

        public void Start()
        {
            FightIteration();
            CleanUpAfterFight();
        }
    }
}
