﻿namespace NewRPGSaga.MainProcesses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NewRPGSaga.Characters;

    public class Round
    {
        public Round(List<Character> characters)
        {
            Characters = characters;
        }

        public List<Character> Characters { get; set; }

        public Fight Fight { get; set; }

        public void RefreshAvailiableCharacters()
        {
            Characters.ForEach(character => character.IsAvailiable = true);
        }

        public void RemoveDeadCharacters()
        {
            Characters.RemoveAll(character => character.IsAlive == false);
        }

        public Character GetAvailiableCharacter()
        {
            Random rand = new Random();
            Character[] availiableCharacters = Characters.Where(c => c.IsAvailiable).ToArray();
            int character = rand.Next(0, availiableCharacters.Length);
            availiableCharacters[character].IsAvailiable = false;
            return availiableCharacters[character];
        }

        public void Pairing(out Character firstFighter, out Character secondFighter)
        {
            firstFighter = GetAvailiableCharacter();
            secondFighter = GetAvailiableCharacter();

            Logger.Log($"Meet our fighters: {firstFighter} vs {secondFighter}\n", ConsoleColor.Blue);
        }

        public void PerformFights()
        {
            for (int i = 0; i < Characters.Count / 2; i++)
            {
                Pairing(out Character firstFighter, out Character secondFighter);
                Fight = new Fight(firstFighter, secondFighter);
                Fight.Start();
            }
        }

        public void Start()
        {
            RefreshAvailiableCharacters();
            PerformFights();
            RemoveDeadCharacters();
        }
    }
}
