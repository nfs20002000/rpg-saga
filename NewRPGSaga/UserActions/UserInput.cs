﻿namespace NewRPGSaga.UserActions
{
    using System;
    using NewRPGSaga;
    using NewRPGSaga.Exceptions;

    public static class UserInput
    {
        public static int HandleCharactersNumberInput(int numberOfCharacters)
        {
            Logger.Log("Please, choose the number of players (it must be even):\n");

            while (numberOfCharacters == -1)
            {
                try
                {
                    numberOfCharacters = Convert.ToInt32(Console.ReadLine());
                    if (numberOfCharacters <= 0 || numberOfCharacters % 2 == 1)
                    {
                        throw new NonValidNumberException();
                    }
                }
                catch (NonValidNumberException)
                {
                    Logger.Log("Please, try again.");
                    numberOfCharacters = -1;
                }
                catch (FormatException e)
                {
                    Logger.Log(e.Message);
                    Logger.Log("Please, try again.");
                    numberOfCharacters = -1;
                }
            }

            return numberOfCharacters;
        }
    }
}
